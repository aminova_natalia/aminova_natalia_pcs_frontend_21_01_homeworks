'use strict'

const inputLogin = document.getElementsByClassName('login_text-input');
const inputParol = document.getElementsByClassName('parol_text-input');
const button = document.getElementsByClassName('form__button');
const span111 = document.getElementsByClassName('text111');
const span121 = document.getElementsByClassName('text121');
const span131 = document.getElementsByClassName('text131');
const span11 = document.getElementsByClassName('text11');
const span12 = document.getElementsByClassName('text12');
const span13 = document.getElementsByClassName('text13');
const inputCheckbox = document.getElementsByClassName('form__checkbox');
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());}
const loginError = () => {
    inputLogin[0].style.border = '2px solid red';
    span111[0].style.opacity = 1;
    span11[0].style.color = 'red';
};
const ParolError = () => {
    inputParol[0].style.border = '2px solid red';
    span121[0].style.opacity = 1;
    span12[0].style.color = 'red';
};
const сheckboxError = () => {
    span131[0].style.opacity = 1;
    span13[0].style.color = 'red';
    inputCheckbox[0].style.borderColor = 'red';
};

button[0].addEventListener('click', (event) => {
    const userData = {};
    event.preventDefault();
    if (!inputLogin[0].value) {
        loginError();}
    else {
        if (!validateEmail(inputLogin[0].value)) {
            loginError();
            span111[0].innerHTML = 'E-mail не валидный';
        }
        else {userData.email = inputLogin[0].value;}
    }
     if (!inputParol[0].value) {
        ParolError();
    } else {
        if (inputParol[0].value.length < 8) {
            ParolError();
            span121[0].innerHTML = 'Пароль должен содержать как минимум 8 символов'
        } else {
            userData.password = inputParol[0].value};
    }
    if (!inputCheckbox[0].checked) {
        сheckboxError();}
    if (inputLogin[0].value && inputParol[0].value && inputCheckbox[0].checked) {
        console.log (userData)}
   });