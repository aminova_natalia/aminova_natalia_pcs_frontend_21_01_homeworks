const url = 'https://reqres.in/api/users?per_page=12';

const users = fetch(url)
    .then((response) => {
        return response.json();
    })
    .then((body) => {
        console.log('-----------');
        console.log('1.Получить данные всех пользователей из https://reqres.in/api/users');
        console.log('-----------');
        body?.data.forEach((item) => {
            console.log(item);
        })
        console.log('-----------');
        console.log('2.Вывести в консоль фамилии всех пользователей в цикле');
        console.log('-----------');
        body?.data.forEach((item) => {
            console.log(item. last_name);
        })
    users.then((data) => {
        const userlistfiltered = body?.data.filter((item) => {
            return item?.last_name[0] === 'F';
        })
        console.log('-----------');
        console.log('3.Вывести все данные всех пользователей, фамилия которых начинается на "F"');
        console.log('-----------');
        console.log(userlistfiltered);
    });
     users.then((data) => {
         const meta = body?.data.reduce((prev, current) => {
             prev = prev + current.first_name + " " + current.last_name + ', ';
             return prev;
            }, "")
            console.log('-----------');
            console.log("4. Наша база содержит данные следующих пользователей:");
            console.log('-----------');
            console.log(meta);
        });
     users.then((data) => {
         const UserKeys = Object.keys(body?.data[0]);
            console.log('-----------');
            console.log('5.Вывести названия всех ключей в объекте пользователя');
            console.log('-----------');
            console.log(UserKeys);
            console.log('-----------');
     })
});