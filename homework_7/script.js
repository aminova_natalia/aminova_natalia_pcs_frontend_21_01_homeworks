const a = +prompt('Упрощенный калькулятор. Что нужно вычислить?')
const operator = prompt('Введите знак')
const b = +prompt('Введите второе число')

if (!a || a === null) {
    console.log('Первое число не указано' );
} else {
    if (!b || b === null) {
        console.log('Второе число не указано');
    } else {
        if (!operator || operator === null ) {
            console.log('Не введен знак');
        } else {
            if (isNaN(a) || isNaN(b)) {
                console.log('Некорректный ввод чисел');
            } else {
                switch (operator) {
                    case '+':
                        console.log(a + b);
                        break
                    case '*':
                        console.log(a * b);
                        break
                    case '-':
                        console.log(a - b);
                        break
                    case '/':
                        console.log(a / b);
                        break
                    default:
                        console.log('Программа не поддерживает такую операцию');
                        break
                }

            }
        }
    }
}


