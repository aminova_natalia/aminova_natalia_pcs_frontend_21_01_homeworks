'use strict'
let vino = [];
loadsVino ();
let inputName = document.getElementById('name_box');
let inputYear = document.getElementById('year_box');
let textArea = document.getElementById('textArea');
let price = document.getElementById('price_box');

document.getElementById('description-add').onclick = function () {
    event.preventDefault();
    let inputName = document.getElementById('name_box');
    let inputYear = document.getElementById('year_box');
    let textArea = document.getElementById('textArea');
    let price = document.getElementById('price_box');
    let descriptionVino = {
        name: inputName.value,
        year: inputYear.value,
        text: textArea.value,
        price: price.value,
        buy
    }
    console.log(descriptionVino)

    inputName.value = ' ';
    inputYear.value = ' ';
    textArea.value = ' ';
    price.value = ' ';

    vino.push(descriptionVino);
    saveVino();
    showVino();

}

function saveVino () {
    localStorage.setItem('vino', JSON.stringify(vino));
};

function loadsVino () {
    if (localStorage.getItem('vino')) vino = JSON.parse(localStorage.getItem('vino'));
    showVino();
}

function showVino() {
    let newVino = document.getElementById('new-Vino');
    let out = ' ';
    vino.forEach(function (item) {
        out += `<p class="name">${item.name}</p>`;
        out += `<p class="year">${item.year}</p>`;
        out += `<p class="text">${item.text}</p>`;
        out += `<p class="price">${item.price}</p>`;

    });
    newVino.innerHTML = out;
}
